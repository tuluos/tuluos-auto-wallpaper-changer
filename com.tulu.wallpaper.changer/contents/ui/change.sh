#!/bin/bash

gitlab_path=https://gitlab.com/tuluos/tuluos-wallpaper/-/raw/main

#create the wallpaper folder
if [ ! -d $HOME/.wallpaper ];
then
	mkdir -v $HOME/.wallpaper
fi

#first get list.txt from gitlab and store it in ~/.wallpaper foder
list_path=$gitlab_path"/list.txt"
wget -O $HOME/.wallpaper/list.txt $list_path

# check if the list.txt file is downloaded or not
if [ -f $HOME/.wallpaper/list.txt ];
then

    #get number of wallpapers
    num_wallpaper=$(cat $HOME/.wallpaper/list.txt | wc -l)

    #get previous wallpaper location for plasma cofig
    prev_wallpaper_loc=$(grep -oP "Image=\K.*" $HOME/.config/plasma-org.kde.plasma.desktop-appletsrc | cut -c 8-)
    echo $prev_wallpaper_loc

    #get previous wallpaper name
    prev_wallpaper=$(echo $prev_wallpaper_loc | rev | cut -d '/' -f 1 | rev)
    echo $prev_wallpaper

    #get random no b/w num_wallpaper to select a wallpaper
    file_no=$((1 + $RANDOM % $num_wallpaper))
    echo $file_no

    #get new wallpaper name
    new_wallpaper=$(sed $file_no'!d' $HOME/.wallpaper/list.txt)
    echo $new_wallpaper

    #if new wallpaper name same as previous wallpaper then random generate again
    while [[ "$prev_wallpaper" == "$new_wallpaper" ]];
    do
        file_no=$((1 + $RANDOM % $num_wallpaper))
        new_wallpaper=$(sed $file_no'!d' list.txt)
    done

    new_wallpaper_path=$gitlab_path"/wallpapers/"$new_wallpaper
    echo $new_wallpaper_path

    # get new wallpaper in /tmp directory
    wget -O /tmp/$new_wallpaper $new_wallpaper_path

    # check if file is downloaded
    if [ -f /tmp/$new_wallpaper ];
    then
        # remove all files in $HOME/.wallpaper folder
        rm -rv $HOME/.wallpaper/*

        # move new wallpaper to $HOME/.wallpaper folder
        mv -v /tmp/$new_wallpaper $HOME/.wallpaper/

        #apply new wallpaper
        qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript "
        var allDesktops = desktops();
        var filepath = 'file://'+'$HOME'+'/.wallpaper/'+'$new_wallpaper';
        print (allDesktops);
        for (i=0;i<allDesktops.length;i++) {{
            d = allDesktops[i];
            d.wallpaperPlugin = 'org.kde.image';
            d.currentConfigGroup = Array('Wallpaper',
                                            'org.kde.image',
                                            'General');
            d.writeConfig('Image', filepath)
        }}
        "
    fi
fi
